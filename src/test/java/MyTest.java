import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyTest {

    @Test
    public void testGoogleSearch() {

        System.out.println ("The Test started.");
        // Set the path to Chromedriver executable
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized"); // open Browser in maximized mode
        options.addArguments("disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        //options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://google.com");



        // Open Google homepage
        driver.get("https://www.google.com");

        // Print page title
        System.out.println("  Page title is :   " + driver.getTitle());

        // Close the browser
        driver.quit();
    }
}
