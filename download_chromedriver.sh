#!/bin/bash

# Define Chromedriver version
CHROMEDRIVER_VERSION="98.0.4758.102"

# Define Chromedriver URL
CHROMEDRIVER_URL="https://chromedriver.storage.googleapis.com/${CHROMEDRIVER_VERSION}/chromedriver_linux64.zip"

# Create directory to store Chromedriver
mkdir -p chromedriver

# Download and extract Chromedriver
curl -SL "${CHROMEDRIVER_URL}" -o chromedriver.zip
unzip chromedriver.zip -d chromedriver